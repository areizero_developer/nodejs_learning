// Adding a module express (The NPM Framework module)
var express = require('express');

//like create sever in express
var app = express();	//or express.createServer();

app.get('/', function (req, response) //using in ex`press to acces server from /
{
    // with the http response write a visible message in server
    response.send('Hello world from express Framework\n');

});
app.listen(1337); //using server.listen(handle[, callback])

// just for log in console
console.log('Server running at 127.0.0.1:1337 or localhost:1337');