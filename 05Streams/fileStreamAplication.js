/*Stream are object to read and write data in a continus form*/

//requiering FileStream module
var fs = require('fs');

/*Stream types in node: Readable, Writable, Duplex, Transform*/
var readStream = fs.createReadStream('input.txt');
var writeStream = fs.createWriteStream('output.txt');

/*Each type of Stream is an EventEmitter instance with events: data, end, error, finish*/
copyReadToWriteStream(readStream, writeStream);

/*using piping we can provide output of a stream like the input of other stream*/
var readStream2 = fs.createReadStream('input2.txt');
var writeCopyStream = fs.createWriteStream('inputCopy.txt');
console.log("Copy complete input.txt to inputCopy.txt using pipe");
readStream2.pipe(writeCopyStream);

/*Chaining stream with pipe is a common practice in node js*/
var zlib = require('zlib');
var gzip = zlib.createGzip();
//compress the file
console.log("Compressing input.txt");
fs.createReadStream('input.txt').pipe(gzip).pipe(fs.createWriteStream('input.txt.gz'));

console.log("End FileStream");

/*using events of Stream*/
function copyReadToWriteStream(rStr, wStr)
{
	var data = '';
	//call event data
	rStr.on('data', function(part){
		data += part;
	});

	//call event end
	rStr.on('end',function(){
		console.log("Ending copy ReadStream to writeStream; Data:");
   		console.log(data);
	});

	//call event data
	rStr.on('error', function(err){
		console.log("Wild error has appeared");
		console.log(err.stack);
	});

	//writing data with UTF8
	var predata = "This content is before the copy data\n";
	wStr.write(predata.toUpperCase(), 'UTF8');
	wStr.write(data, 'UTF8');
	//mark end of file (avoid scape of data)
	wStr.end;

	//event finish
	wStr.on('finish', function() {
    	console.log("Write completed");
	});

	//event error
	wStr.on('error', function(err){
   		console.log(err.stack);
	});
}

//MY OWN CHALLANGE, DO COMPRESSING TO VARIOUS FILES