// Adding a module from Node (https://nodejs.org/api/modules.html#modules_modules)
var http = require('http');

// create a server or webserver from the module http (http.createServer([requestListener]))
var server = http.createServer(function (req, response)
{
    // with the http response write a iible message in server
    response.writeHead(200, {'Content-Type': 'text/plain'});
    response.end('Message in server from node\n');
 
});
 
server.listen(1337);

// just for log in console
console.log('Server running at ec2-52-39-177-30.us-west-2.compute.amazonaws.com:1337');