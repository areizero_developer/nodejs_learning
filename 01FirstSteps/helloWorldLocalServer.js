// Adding a module from Node (https://nodejs.org/api/modules.html#modules_modules)
var http = require('http');

// create a server or webserver from the module http (http.createServer([requestListener]))
http.createServer(function (req, response) 
{
    // with the http response write a iible message in server
    response.writeHead(200, {'Content-Type': 'text/plain'});
    response.end('Message in server from node\n');

}).listen(1337, '127.0.0.1');	//using server.listen(handle[, callback])

// just for log in console
console.log('Server running at 127.0.0.1:1337 or localhost:1337');