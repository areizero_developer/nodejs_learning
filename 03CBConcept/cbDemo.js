//Modulo File System
var fs = require("fs");

//leemos el archivo y llamamo la función de CallBack, fs.readFile() is a async function
fs.readFile('txtFile.txt', function (err, data) 
{
	//any async function accepts a callback as a last parameter and the callback function accepts error as a first parameter
    if (err) return console.error(err);	//retornamos en caso de que un error suceda
    console.log(data.toString());	//escribimos lo que hay dentro del archivo
});

console.log("Program Ended");