//event module
var eventMod = require('events');

//event emmiter object
var emi = new eventMod.EventEmitter();
/*EventEmmiter is used for events and eventListener follow Observer pattern*/

//creating an event with anonimous function
emi.on('msgComplete', function(){
	console.log("Message complety sended");
});

//creating a function for release in an event
var sendMsgHandler = function sendMessage()
{
	console.log('Sending a message: ');

	//fire the event
	emi.emit('msgComplete');
}

//setting event listener, on and addListener are similar events -> see: http://www.tutorialspoint.com/nodejs/nodejs_event_emitter.htm
emi.addListener('sendMsg', sendMsgHandler);
//fire event
emi.emit('sendMsg');

console.log("End of Event Loop EventEmmiter example");

/*
NT:
Deferent of CallBacks, events can be executed syncronous with emmiter
*/
